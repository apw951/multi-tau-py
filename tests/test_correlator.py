"""
Test Correlator functionality
"""

from collections.abc import Iterable

import numpy as np

from multitau.correlator import Correlator


def test_setup():
    """
    Test Correlator instancing
    """
    cor = Correlator(blocks=32, points=16, window=2)

    correlation, lags = cor.get()

    assert len(lags) == 0
    assert len(correlation) == 0


def correlate(x: Iterable[float], y: Iterable[float], lags: int) -> Iterable[float]:
    """
    Direct correlation
    """
    n = len(x)
    if not n == len(y):
        raise Warning("Arrays do not have the same size")

    n = min(n, len(y))

    cor = np.zeros(lags)
    counts = np.zeros(lags).astype(int)

    for i in range(n):
        for j in range(lags):
            if i + j < n:
                cor[j] += x[i] * y[i + j]
                counts[j] += 1

    for i in range(lags):
        cor[i] /= max(1, counts[i])

    return cor


def test_correlation():
    """
    Test correlator result is the same as the direct method
    """
    n = 100
    signal = np.exp(-np.linspace(0.0, 1.0, n))

    cor = Correlator(blocks=1, points=n, window=1)
    for x in signal:
        cor.update(x, x)
    actual, lags = cor.get()
    actual /= actual[0]

    expected = correlate(signal, signal, lags=n)
    expected /= expected[0]

    assert np.mean((expected - actual) ** 2) < 1e-16
    assert all(lags == range(n))
