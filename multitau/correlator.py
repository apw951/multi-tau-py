"""
Module to correlate real scalar data
"""

import numpy as np


class Correlator:
    """
    Correlate scalar real values

    Parameters
    ----------
    blocks : int
        Number of correlation blocks
    points : int
        Number of points per block
    window : int
        Averaging window per block level
    """

    # pylint: disable=too-many-instance-attributes

    def __init__(self, *, blocks: int, points: int, window: int):

        self._blocks = blocks
        self._points = points
        self._window = window
        self._max_block_used = 0
        self._min_dist = self._points / self._window

        self._accumulator = np.zeros((self._blocks, 2))
        self._count_accumulated = np.zeros(self._blocks).astype(int)
        self._shift_index = np.zeros(self._blocks).astype(int)
        self._shift = np.zeros((self._blocks, self._points, 2))
        self._shift_not_null = np.zeros((self._blocks, self._points)).astype(bool)
        self._correlation = np.zeros((self._blocks, self._points))
        self._count_correlated = np.zeros((self._blocks, self._points)).astype(int)

    def update(self, a: float, b: float):
        """
        Update the correlation with new values a and b
        """
        self._add(a, b, 0)

    def _add(self, a: float, b: float, block: int):
        """
        Propagate update down block hierarchy
        """
        if block == self._blocks:
            return

        shift = self._shift_index[block]
        self._max_block_used = max(self._max_block_used, block)
        self._shift[block, shift, :] = a, b
        self._accumulator[block, :] += a, b
        self._shift_not_null[block, shift] = True
        self._count_accumulated[block] += 1

        if self._count_accumulated[block] == self._window:
            self._add(
                self._accumulator[block, 0] / self._window,
                self._accumulator[block, 1] / self._window,
                block + 1,
            )
            self._accumulator[block, :] = 0.0
            self._count_accumulated[block] = 0

        i = self._shift_index[block]
        if block == 0:
            j = i
            for n in range(self._points):
                if self._shifts_valid(block, i, j):
                    self._correlation[block, n] += (
                        self._shift[block, i, 0] * self._shift[block, j, 1]
                    )
                    self._count_correlated[block, n] += 1
                j -= 1
                if j < 0:
                    j += self._points
        else:
            for n in range(self._min_dist, self._points):
                if j < 0:
                    j = j + self._points
                if self._shifts_valid(block, i, j):
                    self._correlation[block, n] += (
                        self._shift[block, i, 0] * self._shift[block, j, 1]
                    )
                    self._count_correlated[block, n] += 1
                j = j - 1
        self._shift_index[block] = (self._shift_index[block] + 1) % self._points

    def _shifts_valid(self, block: int, pi: int, pj: int):
        return self._shift_not_null[block, pi] and self._shift_not_null[block, pj]

    def get(self):
        """
        Obtain the correlation and lag times
        """
        correlation = np.zeros(self._points * self._blocks)
        lags = np.zeros(self._points * self._blocks)

        t = 0
        for i in range(self._points):
            if self._count_correlated[0, i] > 0:
                correlation[t] = self._correlation[0, i] / self._count_correlated[0, i]
                lags[t] = i
                t += 1
        for k in range(1, self._max_block_used):
            for i in range(self._min_dist, self._points):
                if self._count_correlated[k, i] > 0:
                    correlation[t] = (
                        self._correlation[k, i] / self._count_correlated[k, i]
                    )
                    lags[t] = float(i) * float(self._window) ** k
                    t += 1
        return (correlation[0:t], lags[0:t])
